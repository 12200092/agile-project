
const app = require('./app')
const port = 5000;

const  mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config({path:'./config.env'})

const DB = process.env.database_url


mongoose.connect(DB).then((con)=>{
    console.log(con.connections);
    console.log('DB connection successful')
}).catch(error => console.log(error));

app.listen(port,()=>{
    console.log(`App is running on ${port}...`)
})